# gitlab-deploy-cli

Deploy gitlab project from the command line.

`Syntax: deploy [-p|a|i|e]`

`options:`

`p   Project ID in gitlab ( Required ).`

`a   Application Name ( Only specified for a project which includes multiple applications ).`

`i   Institution to deploy to ( Only required when application name is provided ).`

`e   Environment to deploy to ( Required ).`

Error checking should keep you out of most trouble.  You will need to supply a project ID which you can copy from the gitlab project page by clicking right under the project name.

You will need a private token in ~/.gitlabprivatetoken.  You can create a token through your account settings.

When going against the surfliner monorepo, you will need to specify the the application name, the target institution and the environment.

