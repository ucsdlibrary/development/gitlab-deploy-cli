#!/bin/bash

CURRENTPAGE=1
PRIVATETOKENFILE=~/.gitlabprivatetoken
DEBUG="FALSE"

#####
## Functions
#####

help()
{
   # Display Help
   echo "Deploy gitlab project."
   echo
   echo "Syntax: deploy [-p|a|i|e]"
   echo "options:"
   echo "p     Project ID in gitlab. ( Required )"
   echo "a     Application Name ( Only specified for a project which includes multiple applications. )"
   echo "i     Institution to deploy to."
   echo "e     Environment to deploy to."
   echo ""
   echo "You will need a private token in ~/.gitlabprivatetoken"
}

#####
## Command arg checks
#####

if [ -z $1 ]; then
    help
    exit
fi

while getopts :p:a:i:e: flag
do
    case "${flag}" in
        p) PROJECTID=${OPTARG};;
        a) APPLICATION=`echo ${OPTARG} | tr '[:upper:]' '[:lower:]'`;;
        i) INSTITUTION=`echo ${OPTARG} | tr '[:upper:]' '[:lower:]'`;;
        e) ENVIRONMENT=`echo ${OPTARG} | tr '[:upper:]' '[:lower:]'`;;
        *)
          help
          exit;;
    esac
done

if [ "$DEBUG" == "TRUE" ]; then
    echo "---- Parameters ---- "
    echo "PROJECTID: $PROJECTID"
    echo "APPLICATION: $APPLICATION"
    echo "INSTITUTION: $INSTITUTION"
    echo "ENVIRONMENT: $ENVIRONMENT"
fi

#####
## We need to get a token from ~/.gitlabprivatetoken
#####

if [ -r "$PRIVATETOKENFILE" ]; then
    PRIVATETOKEN=`cat ~/.gitlabprivatetoken`
else
    echo "You must have a private token in ~/.gitlabprivatetoken."
    exit
fi

#####
## Parameter checking
#####

if [ -z $PROJECTID ]; then
    echo "You must supply a project ID with the -p flag."
    exit
fi

if [ -z $APPLICATION ]; then
    APPLICATION=`curl --silent --header "PRIVATE-TOKEN: $PRIVATETOKEN" \
      "https://gitlab.com/api/v4/projects/$PROJECTID" \
      | jq .name | tr -d '"' | tr '[:upper:]' '[:lower:]'`
        if [ "$DEBUG" == "TRUE" ]; then
            echo "---- https://gitlab.com/api/v4/projects/$PROJECTID ----"
            echo $APPLICATION
            echo "----"
        fi
    MONOREPOFLAG="FALSE"
else
    MONOREPOFLAG="TRUE"
fi

if [ $MONOREPOFLAG == "TRUE" ] && [ -z $INSTITUTION ]; then
    echo "You must specify an institution and application for the monorepo."
    exit
fi

if [ -z $ENVIRONMENT ]; then
    echo "You must specify an environment to deploy to ( production or staging )"
    exit
fi

#####
## Find the most recent pipeline for this project/environment or project/application/institution/environment/deploy combo.
#####

while [ "$DEPLOYMENTFOUND" != "TRUE" ]; do
    # Get a page of pipelines
    PIPELINELIST=`curl --silent --header "PRIVATE-TOKEN: $PRIVATETOKEN" \
        "https://gitlab.com/api/v4/projects/$PROJECTID/pipelines?order_by=id&sort=desc&per_page=50&page=$CURRENTPAGE" \
        | jq '.[] | .id'`
    if [ "$DEBUG" == "TRUE" ]; then
        echo "---- https://gitlab.com/api/v4/projects/$PROJECTID/pipelines?order_by=id&sort=desc&per_page=50&page=$CURRENTPAGE ----"
        echo $PIPELINELIST
        echo "----"
    fi
    if [ "$PIPELINELIST" = "" ]; then
        echo "Deployment job for $APPLICATION $ENVIRONMENT not found in pipeline list."
        exit
    fi
    # Get the list of jobs for each pipeline
    for PIPELINE in $PIPELINELIST; do
        echo "Checking for match in pipe $PIPELINE"
        JOBLIST=`curl --silent --header "PRIVATE-TOKEN: $PRIVATETOKEN" "https://gitlab.com/api/v4/projects/$PROJECTID/pipelines/$PIPELINE/jobs" \
            | jq '.[] | .id'`
        if [ "$DEBUG" == "TRUE" ]; then
            echo "---- https://gitlab.com/api/v4/projects/$PROJECTID/pipelines/$PIPELINE/jobs ----"
            echo $JOBLIST
            echo "----"
        fi

        # Grab the info for each job
        for JOB in $JOBLIST; do
            JOBNAME=`curl --silent --header "PRIVATE-TOKEN: $PRIVATETOKEN" \
            "https://gitlab.com/api/v4/projects/$PROJECTID/jobs/$JOB" \
            | jq .name | tr -d '"'`
            if [ "$DEBUG" == "TRUE" ]; then
                echo "---- https://gitlab.com/api/v4/projects/$PROJECTID/jobs/$JOB ----"
                echo $JOBNAME
                echo "----"
            fi

            # Check to see if the name of the job matches our search criteria for project/application/environment
            if [[ "$JOBNAME" =~ .*"$APPLICATION".* ]]; then
                echo "Found application: $APPLICATION"
                if [[ "$JOBNAME" =~ .*"$ENVIRONMENT".* ]]; then
                    echo "Found environment: $ENVIRONMENT"
                    # In the monorepo we also need to match the institution and look for a deploy
                    if [[ $MONOREPOFLAG == "TRUE" ]] && [[ "$JOBNAME" =~ .*"$INSTITUTION".* ]] && [[ "$JOBNAME" =~ .*"deploy".* ]]; then
                        RETRYJOB=`curl --silent --request POST --header "PRIVATE-TOKEN: $PRIVATETOKEN" \
                            "https://gitlab.com/api/v4/projects/$PROJECTID/jobs/$JOB/retry"`
                        if [ "$DEBUG" == "TRUE" ]; then
                            echo $RETRYJOB
                        fi
                        if [[ "$RETRYJOB" =~ .*"Job is not retryable".* ]]; then
                            echo "Job is not retryable"
                        else
                            echo "Deploy job started - Check project jobs for progress."
                            exit
                        fi
                    elif [[ $MONOREPOFLAG == "FALSE" ]]; then
                        RETRYJOB=`curl --silent --request POST --header "PRIVATE-TOKEN: $PRIVATETOKEN" \
                            "https://gitlab.com/api/v4/projects/$PROJECTID/jobs/$JOB/retry"`
                        if [ "$DEBUG" == "TRUE" ]; then
                            echo $RETRYJOB
                        fi
                        if [[ "$RETRYJOB" =~ .*"Job is not retryable".* ]]; then
                            echo "Job is not retryable"
                        else
                            echo "Deploy job started - Check project jobs for progress."
                            exit
                        fi
                    fi
                fi
            fi
            
        done

    done

    ((CURRENTPAGE++))
done